/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ZDCTPCNV_ZDCTPCNVDICT_H
#define ZDCTPCNV_ZDCTPCNVDICT_H

#include "ZdcEventTPCnv/ZdcDigits_p1.h"
#include "ZdcEventTPCnv/ZdcDigitsCollection_p1.h"
#include "ZdcEventTPCnv/ZdcRawChannel_p1.h"
#include "ZdcEventTPCnv/ZdcRawChannelCollection_p1.h"
#include "ZdcEventTPCnv/ZDC_SimFiberHit_p1.h"
#include "ZdcEventTPCnv/ZDC_SimFiberHit_Collection_p1.h"

namespace GCCXML_DUMMY_INSTANTIATION_ZDCEVENTTPCNV {
  std::vector<ZdcDigits_p1>                        zdc0;
  std::vector<ZdcRawChannel_p1>                    zdc1;
  std::vector<ZDC_SimFiberHit_p1>                  zdc2;
  std::vector<ZdcDigits_p1>                        zdc3;
  std::vector<ZdcRawChannel_p1>                    zdc4;

}

#endif 
