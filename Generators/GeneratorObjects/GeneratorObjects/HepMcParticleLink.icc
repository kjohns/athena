/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file GeneratorObjects/HepMcParticleLink.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Feb, 2019
 * @brief a link optimized in size for a GenParticle in a McEventCollection
 */


//**************************************************************************
// ExtendedBarCode
//

inline
HepMcParticleLink::ExtendedBarCode::ExtendedBarCode()
{
}


/**
 * @brief Constructor.
 * @param uid Unique ID of target particle.
 * @param eventIndex Identifies the target GenEvent in a McEventCollection,
 *        as either the event number if @c isIndexEventPosition is IS_EVENTNUM,
 *        or the position in the container
 *        if isIndexEventPosition is IS_POSITION.
 *        0 always means the first event in the collection.
 * @param isIndexEventPosition: See @c eventIndex.
 */
inline
HepMcParticleLink::ExtendedBarCode::ExtendedBarCode
  (barcode_type uid,
   index_type eventIndex,
   PositionFlag isIndexEventPosition /*=IS_EVENTNUM*/,
   UniqueIDFlag isUniqueIDBarcode /*= IS_ID*/)
{
  setIndex (eventIndex, isIndexEventPosition);
  setUniqueID (uid, isUniqueIDBarcode);
}


/**
 * @brief Copy constructor.  (Can't be defaulted due to the atomic member.)
 */
inline
HepMcParticleLink::ExtendedBarCode::ExtendedBarCode (const ExtendedBarCode& rhs)
  : m_BC (static_cast<barcode_type> (rhs.m_BC)),
    m_evtIndex (static_cast<index_type> (rhs.m_evtIndex))
{
}


/**
 * @brief Move constructor.  (Can't be defaulted due to the atomic member.)
 */
inline
HepMcParticleLink::ExtendedBarCode::ExtendedBarCode (ExtendedBarCode&& rhs) noexcept
  : m_BC (rhs.m_BC.load()), // no move constructor for atomics
    m_evtIndex (rhs.m_evtIndex.load()) // no move constructor for atomics
{
}

/**
 * @brief Assignment.  (Can't be defaulted due to the atomic member.)
 */
inline
HepMcParticleLink::ExtendedBarCode&
HepMcParticleLink::ExtendedBarCode::operator= (const ExtendedBarCode& rhs)
{
  if (this != &rhs) {
    m_BC = static_cast<barcode_type> (rhs.m_BC);
    m_evtIndex = static_cast<index_type> (rhs.m_evtIndex);
  }
  return *this;
}

/**
 * @brief Assignment.  (Can't be defaulted due to the atomic member.)
 */
inline
HepMcParticleLink::ExtendedBarCode&
HepMcParticleLink::ExtendedBarCode::operator= (ExtendedBarCode&& rhs) noexcept
{
  if (this != &rhs) {
    m_BC = static_cast<barcode_type> (rhs.m_BC.load());
    m_evtIndex = static_cast<index_type>(rhs.m_evtIndex.load());
  }
  return *this;
}


/**
 * @brief Unique ID of target variable (0 for a null link).
 */
inline
HepMcParticleLink::barcode_type
HepMcParticleLink::ExtendedBarCode::uid() const
{
  return m_BC;
}


/**
 * @brief Return the event index/position.
 * @param index[out] Event index (number), or @c UNDEFINED.
 * @param position[out] Event position, or @c UNDEFINED.
 *
 * The GenEvent within the McEventCollection is identified either by
 * the GenEvent number or by the position within the collection.
 * This method will return this by setting either @c index or @c position;
 * the other one is set to @c UNDEFINED.
 */
inline
void HepMcParticleLink::ExtendedBarCode::eventIndex (index_type& index,
                                                     index_type& position) const
{
  index_type idx = m_evtIndex;
  if (idx & POSITION_MASK) {
    index = UNDEFINED;
    position = idx & ~POSITION_MASK;
  }
  else {
    index = idx;
    position = UNDEFINED;
  }
}


/**
 * @brief Return the GenParticle id/barcode.
 * @param id[out] GenParticle::id, or @c UNDEFINEDBC.
 * @param barcode[out] barcode (deprecated), or @c UNDEFINEDBC.
 *
 * The GenParticle within the GenEvent is identified either by
 * the GenParticle::id or the barcode.
 * This method will return this by setting either @c id or @c barcode;
 * the other one is set to @c UNDEFINEDBC.
 */
inline
void HepMcParticleLink::ExtendedBarCode::uniqueID (barcode_type& id,
                                                     barcode_type& barcode) const
{
  barcode_type uid = m_BC;
  if (uid == 0) // special case for delta-rays
    {
      id = uid;
      barcode = uid;
    }
  else if (uid & BARCODE_MASK) {
    id = UNDEFINEDBC;
    barcode = uid & ~BARCODE_MASK;
  }
  else {
    id = uid;
    barcode = UNDEFINEDBC;
  }
}


/**
 * @brief Equality test.
 *
 * Be aware: if one EBC holds the target GenEvent by number and the
 * other by position, then this will always return false, even if they
 * reference the same GenEvent.
 * To avoid this, use HepMcParticleLink::operator=.
 */
inline
bool
HepMcParticleLink::ExtendedBarCode::operator==(const ExtendedBarCode& rhs) const
{
  return (this->m_BC == rhs.m_BC &&
          this->m_evtIndex == rhs.m_evtIndex);
}


/**
 * @brief Inequality test.
 *
 * Be aware: if one EBC holds the target GenEvent by number and the
 * other by position, then this will always return true, even if they
 * reference the same GenEvent.
 * To avoid this, use HepMcParticleLink::operator=.
 */
inline
bool
HepMcParticleLink::ExtendedBarCode::operator!= (const ExtendedBarCode& rhs) const
{
  return !(operator==(rhs));
}


/**
 * @brief Ordering test.
 *
 * Be aware: if one EBC holds the target GenEvent by number and the
 * other by position, then this will not work as expected.
 * To avoid this, use HepMcParticleLink::operator=.
 */
inline
bool
HepMcParticleLink::ExtendedBarCode::operator< (const ExtendedBarCode& rhs) const
{
  return (
          (m_evtIndex < rhs.m_evtIndex)  ||
          (m_evtIndex == rhs.m_evtIndex && m_BC < rhs.m_BC) ) ;
}


/**
 * @brief Compare the event index part of two links.
 * @param lhs First link to compare.
 * @param rhs Second link to compare.
 * @returns -1, 0, or 1, depending on the result of the comparison.
 *
 * The event index part of the link can be represented as either
 * an event number or the position within the container.
 * If necessary, the links will be normalized so that they
 * both refer to an event number.
 */
inline
int
HepMcParticleLink::ExtendedBarCode::compareIndex (const HepMcParticleLink& lhs,
                                                  const HepMcParticleLink& rhs)
{
  // Get the stored indices.  The high bit will be set of they
  // represent a position.  Do a quick test for equality.
  index_type idx1 = lhs.m_extBarcode.m_evtIndex;
  index_type idx2 = rhs.m_extBarcode.m_evtIndex;
  if (idx1 == idx2) return 0;

  // Normalize the values so that they both refer to event number;
  // this happens as a side-effect of calling cptr().
  if (idx1 & POSITION_MASK) {
    lhs.cptr();
    idx1 = lhs.m_extBarcode.m_evtIndex;
  }
  if (idx2 & POSITION_MASK) {
    rhs.cptr();
    idx2 = rhs.m_extBarcode.m_evtIndex;
  }

  // Compare.
  if (idx1 == idx2) {
    return 0;
  }
  else if (idx1 < idx2) {
    return -1;
  }
  else {
    return 1;
  }
}



/**
 * @brief Compare the unique ID part of two links.
 * @param lhs First link to compare.
 * @param rhs Second link to compare.
 * @returns -1, 0, or 1, depending on the result of the comparison.
 *
 * The unique ID part of the link can be represented as either
 * a barcode or the id.
 * If necessary, the links will be normalized so that they
 * both refer to an id.
 */
inline
int
HepMcParticleLink::ExtendedBarCode::compareUniqueID (const HepMcParticleLink& lhs,
                                                  const HepMcParticleLink& rhs)
{
  // Get the stored indices.  The high bit will be set of they
  // represent a position.  Do a quick test for equality.
  barcode_type uid1 = lhs.m_extBarcode.m_BC;
  barcode_type uid2 = rhs.m_extBarcode.m_BC;
  if (uid1 == uid2) return 0;

  // Normalize the values so that they both refer to the id;
  // this happens as a side-effect of calling cptr().
  if (uid1 & BARCODE_MASK) {
    lhs.cptr();
    uid1 = lhs.m_extBarcode.m_BC;
  }
  if (uid2 & BARCODE_MASK) {
    rhs.cptr();
    uid2 = rhs.m_extBarcode.m_BC;
  }
  // Compare.
  if (uid1 == uid2) {
    return 0;
  }
  else if (uid1 < uid2) {
    return -1;
  }
  else {
    return 1;
  }
}


/**
 * @brief Change m_BC from barcode to ID.
 * @param ID GenParticle::id value to set.
 * @param barcode existing barcode value.
 *
 * If the link is currently referencing a GenParticle with @c barcode,
 * update it so that it instead references the GenParticle
 * with id value @c ID.
 *
 * This may be called concurrently, as long as all such concurrent
 * calls have the same arguments.
 */
inline
void HepMcParticleLink::ExtendedBarCode::makeID (barcode_type ID,
                                                 barcode_type barcode) const
{
  assert ((ID & BARCODE_MASK) == 0);
  barcode_type old = barcode | BARCODE_MASK;
  m_BC.compare_exchange_strong (old, ID);
  assert (old == (barcode|BARCODE_MASK) || old == ID);
}


/**
 * @brief Change index from position to number.
 * @param index Event number to set.
 * @param position Existing event position.
 *
 * If the link is currently referencing the GenEvent at @c position,
 * update it so that it instead references the GenEvent
 * with number @c index.
 *
 * This may be called concurrently, as long as all such concurrent
 * calls have the same arguments.
 */
inline
void HepMcParticleLink::ExtendedBarCode::makeIndex (index_type index,
                                                    index_type position) const
{
  assert ((index & POSITION_MASK) == 0);
  index_type old = position | POSITION_MASK;
  m_evtIndex.compare_exchange_strong (old, index);
  assert (old == (position|POSITION_MASK) || old == index || (position==UNDEFINED && old==0));
}


/**
 * @brief Initialize the unique identifier part of the link.
 * @param uid The id or barcode.
 * @param barcodeFlag If IS_BARCODE, @c uid represents a GenParticle barcode (deprecated);
 *        otherwise, it represents a GenParticle::id().
 */
inline
void
HepMcParticleLink::ExtendedBarCode::setUniqueID (barcode_type uid,
                                              UniqueIDFlag barcodeFlag)
{
  assert ((uid & BARCODE_MASK) == 0);
  // For delta-rays barcode=id=0
  if (uid != 0 && barcodeFlag == IS_BARCODE) {
    uid |= BARCODE_MASK;
  }
  m_BC = uid;
}


/**
 * @brief Initialize the event index part of the link.
 * @param idx The index or position.
 * @param positionFlag If IS_POSITION, @c idx represents a position
 *        in the collection; otherwise, it represents an event number.
 */
inline
void
HepMcParticleLink::ExtendedBarCode::setIndex (index_type idx,
                                              PositionFlag positionFlag)
{
  assert ((idx & POSITION_MASK) == 0);
  if (positionFlag == IS_POSITION) {
    idx |= POSITION_MASK;
  }
  m_evtIndex = idx;
}

inline bool HepMcParticleLink::ExtendedBarCode::linkIsNull() const
{
  barcode_type particle_id, particle_barcode;
  uniqueID (particle_id, particle_barcode);
  return (
          (particle_id == 0 && particle_barcode == 0) || // delta rays
          (particle_id == 0 && particle_barcode == ExtendedBarCode::UNDEFINEDBC) ||
          (particle_id  == ExtendedBarCode::UNDEFINEDBC && particle_barcode == 0)
          );
}


//**************************************************************************
// HepMcParticleLink
//


/**
 * @brief Default constructor.  Makes a null link.
 * @param sg Optional specification of a specific store to reference.
 */
inline
HepMcParticleLink::HepMcParticleLink (IProxyDict* sg /*= nullptr*/)
  : m_store (sg)
{
}


/**
 * @brief Default constructor.  Makes a null link.
 * @param ctx Context of the store to reference.
 */
inline
HepMcParticleLink::HepMcParticleLink (const EventContext& ctx)
  : HepMcParticleLink (Atlas::getExtendedEventContext(ctx).proxy())
{
}

/**
 * @brief Constructor.
 * @param uid Unique ID of the target particle.  0 means a null link.
 * @param eventIndex Identifies the target GenEvent in a McEventCollection,
 *        as either the event number if @c isIndexEventPosition is IS_EVENTNUM,
 *        or the position in the container
 *        if isIndexEventPosition is IS_POSITION.
 *        0 always means the first event in the collection.
 * @param positionFlag: See @c eventIndex.
 * @param sg Optional specification of a specific store to reference.
 */
inline
HepMcParticleLink::HepMcParticleLink (barcode_type uid,
                                      uint32_t eventIndex /*= 0*/,
                                      PositionFlag positionFlag /*= IS_EVENTNUM*/,
                                      UniqueIDFlag uniqueIDFlag /*= IS_ID*/,
                                      IProxyDict* sg /*= SG::CurrentEventStore::store()*/)
  : m_store (sg),
    m_extBarcode (uid, eventIndex, positionFlag, uniqueIDFlag)
{
}


/**
 * @brief Constructor.
 * @param uid Unique ID of the target particle.  0 means a null link.
 * @param eventIndex Identifies the target GenEvent in a McEventCollection,
 *        as either the event number if @c isIndexEventPosition is IS_EVENTNUM,
 *        or the position in the container
 *        if isIndexEventPosition is IS_POSITION.
 *        0 always means the first event in the collection.
 * @param positionFlag: See @c eventIndex.
 * @param ctx Context of the store to reference.
 */
inline
HepMcParticleLink::HepMcParticleLink (barcode_type uid,
                                      uint32_t eventIndex,
                                      PositionFlag positionFlag,
                                      UniqueIDFlag uniqueIDFlag,
                                      const EventContext& ctx)
  : HepMcParticleLink (uid, eventIndex, positionFlag, uniqueIDFlag,
                       Atlas::getExtendedEventContext(ctx).proxy())
{
}


/**
 * @brief Constructor.
 * @param p Particle to reference.
 * @param eventIndex Identifies the target GenEvent in a McEventCollection,
 *        as either the event number if @c isIndexEventPosition is IS_EVENTNUM,
 *        or the position in the container
 *        if isIndexEventPosition is IS_POSITION.
 *        0 always means the first event in the collection.
 * @param positionFlag: See @c eventIndex.
 * @param ctx Context of the store to reference.
 */
inline
HepMcParticleLink::HepMcParticleLink (const HepMC::ConstGenParticlePtr& part,
                                      uint32_t eventIndex,
                                      PositionFlag positionFlag,
                                      const EventContext& ctx)
  : HepMcParticleLink (part, eventIndex, positionFlag,
                       Atlas::getExtendedEventContext(ctx).proxy())
{
}

/**
 * @brief Dereference.
 */
inline
const HepMC::GenParticle& HepMcParticleLink::operator* () const
{
  return *cptr();
}


/**
 * @brief Dereference.
 */
inline
HepMC::ConstGenParticlePtr HepMcParticleLink::operator->() const
{
  return cptr();
}


/**
 * @brief Dereference.
 */
inline
HepMcParticleLink::operator HepMC::ConstGenParticlePtr() const
{
  return cptr();
}


/** 
 * @brief Validity check.  Dereference and check for null.
 */
inline
bool HepMcParticleLink::isValid() const
{
  return (nullptr != cptr());
}


/** 
 * @brief Validity check.  Dereference and check for null.
 */
inline
bool HepMcParticleLink::operator!() const
{
  return !isValid();
}


/** 
 * @brief Validity check.  Dereference and check for null.
 */
inline
HepMcParticleLink::operator bool() const
{
  return isValid();
}


/**
 * @brief Equality comparison.
 */
inline
bool HepMcParticleLink::operator== (const HepMcParticleLink& rhs) const
{
  return  (ExtendedBarCode::compareUniqueID(*this, rhs) == 0 &&
           ExtendedBarCode::compareIndex (*this, rhs) == 0);
}


/**
 * @brief Inequality comparison.
 */
inline
bool HepMcParticleLink::operator!= (const HepMcParticleLink& rhs) const
{
  return !(operator==(rhs));
}


/**
 * @brief Ordering comparison.
 */
inline
bool HepMcParticleLink::operator< (const HepMcParticleLink& rhs) const
{
  int cmpIndex = ExtendedBarCode::compareIndex (*this, rhs);
  if (cmpIndex < 0) return true;
  if (cmpIndex == 0) {
    if (ExtendedBarCode::compareUniqueID(*this, rhs) < 0) return true;
  }
  return false;
}


/**
 * @brief Hash the 32-bit barcode and 16-bit eventindex into a 32bit int.
 */
inline
HepMcParticleLink::barcode_type HepMcParticleLink::compress() const
{
  return ( ((m_extBarcode.uid()&0xFFFF) << 16) |
           eventIndex() );
}


/**
 * @brief Comparison with ConstGenParticlePtr.
 * Needed with c++20 to break an ambiguity.
 */
inline
bool operator== (HepMC::ConstGenParticlePtr a,
                 const HepMcParticleLink& b)
{
  return a == b.cptr();
}
